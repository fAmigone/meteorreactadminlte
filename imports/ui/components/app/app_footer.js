import React from 'react';

const AppFooter = () => (
  <footer className="main-footer">
    <div className="pull-right hidden-xs">
      <b>Version</b> 2.3.11
    </div>
    <strong>Copyright &copy;  <a href="http://reactib.com">REACTIB</a> - </strong> React With Us
  </footer>
);

export default AppFooter;
